<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dados Acadêmicos</title>
</head>
<body>
    <h1>Dados Acadêmicos</h1>
    <h3> Aluno: Gustavo William de Assunção Ribeiro - Turma: AUT2D3 </h3>
    <hr>

    <?php
        require "bd/dados.php";
        $id = 1;
        try {
            $conexao = new PDO('mysql:host=localhost;dbname=academico', $username, $password,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'') );
            $query = $conexao->prepare('SELECT * FROM alunos WHERE id = :id ');
            $query->execute(array('id' => $id));

            $resultado = $query->fetchAll();

            if ( count($resultado) ) {
                foreach($resultado as $row) {
                    print_r($row);
                    echo '<br> <br>';
                }
            } else {
                echo "Nennhum resultado retornado.";
            }

            echo "<hr>";

            $query = $conexao->prepare('SELECT * FROM faltas WHERE id_aluno = :id ');
            $query->execute(array('id' => $id));

            $resultado = $query->fetchAll();

            if ( count($resultado) ) {
                foreach($resultado as $row) {
                    var_dump($row);
                    echo '<br> <br>';
                    echo 'id_aluno: '.$row['id_aluno'].', mes: '.$row['mes'].', faltas: '.$row['faltas'];
                    echo '<br> <br>';
                }
            } else {
                echo "Nennhum resultado retornado.";
            }

            // inserir dados 
            // INSERT INTO `alunos` (`id`, `matricula`, `cpf`, `nome`, `email`, `gitlab`, `celular`, `turma`) VALUES (NULL, '234567', '11111111111', 'Joaquim dos Santos', 'joaquim@email.com', 'joaq.santos', '123456789', 'AUT2D2');
           
        } catch(PDOException $e) {
            echo 'ERROR: ' . $e->getMessage();
        }

    ?>
</body>
</html>
